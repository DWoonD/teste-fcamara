import React from 'react';
import './style/cadastrar.css';

import useForm from './hooks/useForm';

import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';


function Cadastrar() {
    let mensagens = [];
    const [{ values, loading }, handleChange, handleSubmit] = useForm();

    const cadastrarMensagem = () => {
        if(localStorage.hasOwnProperty('mensagens')) {
            mensagens = JSON.parse(localStorage.getItem('mensagens'));
        }

        mensagens.push(Object.assign(values, {id: Math.random()*10000 + Date.now(), date: new Date()}));

        localStorage.setItem('mensagens', JSON.stringify(mensagens));
        
    }
    return (
        <div className="cadastro">
            <form onSubmit={handleSubmit(cadastrarMensagem)}>
                <TextField
                    onChange={handleChange}
                    name="nome"
                    className="input"
                    required
                    id="nome"
                    label="Nome"
                    variant="outlined"
                />
                <TextField
                    onChange={handleChange}
                    name="email"
                    className="input"
                    required
                    id="email"
                    label="Email"
                    type="email"
                    variant="outlined"
                />
                <TextField
                    onChange={handleChange}
                    name="telefone"
                    className="input"
                    id="telefone"
                    label="Telefone"
                    type="number"
                    variant="outlined"
                />
                <TextField
                    onChange={handleChange}
                    name="assunto"
                    className="input"
                    id="assunto"
                    label="Assunto"
                    variant="outlined"
                />
                <TextField
                    onChange={handleChange}
                    name="mensagem"
                    className="input"
                    id="mensagem"
                    label="Mensagem"
                    multiline
                    rows="7"
                    variant="outlined"
                />
                <Button type="submit" className="submit" variant="contained" color="primary">
                    {loading ? "Enviando..." : "Enviar"}
                </Button>
            </form>
        </div>
    );
};

export default Cadastrar;