import React from 'react';
import './style/sobre.css';

function Sobre() {
    return (
        <div className="sobre">
            <aside>
                <img src="https://instagram.fcgh7-1.fna.fbcdn.net/v/t51.2885-15/e35/41468643_606832963046593_1191602974486917447_n.jpg?_nc_ht=instagram.fcgh7-1.fna.fbcdn.net&_nc_cat=101&_nc_ohc=AOW1UOYOzgEAX9U-5wQ&oh=779bf4990ca9ecb3559f60fc2edb6277&oe=5EFE4ECE" />
            </aside>
            <section>
                <h1>Willian Diego Alves dos Santos</h1>
                <h2>Front End Developer - now with Reactjs Too :D</h2>
                <p>Trabalho com desenvolvimento Fron End desde 2011. Comecei basicamente fazendo sites instituicionais com apenas 
                    HTML e CSS. <br></br>
                    Hoje em dia estou acostumado a utilizar o Angular 2+ e depois desse teste, confesso estar apaixonado pelo React. <br></br>
                    Gosto muito de programar, acho que é uma das capacidades humana mais interessantes pois você pode simplesmente 
                    imaginar algo e dar vida a ela. Gosto muito da comunidade de desenvolvimento também, pois graças a ela consegui 
                    vir trabalhar em Sampa e literalmente mudar de vida. <br></br>
                    Ja tive alguns projetos de Mentoria, ajudando uma garota do interior a vir para o mundo front end e atualmente 
                    estou ajudando um amigo que está na faculdade da mesma forma. <br></br>
                    Segue links que julgo ser úteis :D<br></br>
                </p>
                <ul>
                    <li><a target="_blank" href="https://www.linkedin.com/in/williandsantos">Linkedin</a></li>
                    <li><a target="_blank" href="https://github.com/dwoond">Github</a></li>
                    <li><a target="_blank" href="https://dwoond.github.io">Blog</a></li>
                </ul>
            </section>
        </div>
    )
}

export default Sobre;