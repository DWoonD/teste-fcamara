import React, { useState } from 'react';
import Moment from 'react-moment';
import './style/dashboard.css';
import CardMensagem from './card-mensagem';
import Button from '@material-ui/core/Button';

export default function Dashboard() {

    let [selectedMessage, setSelectedMessage] = useState('');

    let [messages, setMessages] = useState(JSON.parse(localStorage.getItem('mensagens')));

    function selectMessage(id) {
        messages.find((message) => {
            if (message.id === id) {
                return setSelectedMessage(message)
            }
            return null;
        })
    }

    function removeMessage(message) {
        messages = messages.filter((item) => {
            if (item.id !== message.id) {
                return item;
            }
            return null;
        })
        localStorage.setItem('mensagens', JSON.stringify(messages));
        setMessages(messages);
        setSelectedMessage('')
    }

    function nenhumaMensage() {
        return <li>Nenhuma mensagem cadastrada ainda!</li>
    }

    return (
        <div className="dashboard" >
            <aside className="mensagens">
                <ul>
                    {messages !== null ? (
                        messages.map(message => (
                            <li key={message.id} onClick={() => selectMessage(message.id)}>
                                <CardMensagem assunto={message.assunto} date={message.date}></CardMensagem>
                            </li>
                        ))) : (
                            nenhumaMensage()
                        )}
                </ul>
            </aside>
            {selectedMessage !== '' ? (
                <section>
                    <div className="mensagem">
                        <h2>{selectedMessage.assunto}</h2>
                        <Moment format="DD/MM/YYYY HH:mm">
                            {selectedMessage.date}
                        </Moment>
                        <p>{selectedMessage.mensagem}</p>
                    </div>
                    <div className="excluirBtn">
                        <Button onClick={() => removeMessage(selectedMessage)} variant="contained" color="secondary">
                            Excluir
                        </Button>
                    </div>
                </section>
            ) : (
                <span></span>
                )}
        </div>
    );
};
