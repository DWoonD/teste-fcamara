import React, { useState, useEffect } from 'react';
import { Switch, Route, Link } from 'react-router-dom';

import './style/App.css';

import Home from './home';
import Cadastrar from './cadastrar';
import Dashboard from './dashboard';
import Sobre from './sobre';

import axios from 'axios';

function App() {
  let [menu, setMenu] = useState([]);

  useEffect(() => {
    async function loadMenu() {
      const response = await axios.get('./mock/menu.json');

      setMenu(response.data.menu);
    }

    loadMenu();
  }, []);
  return (

    <div className="app">
      <header className="app-header">
        <ul>
          <li className="logo">Teste FCamara - ReactJS</li>
          {menu.map(item => (
            <li  key={item.id}><Link to={item.rota}>{item.titulo}</Link></li>
          ))}
        </ul>
      </header>
      <section className="app-body">
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/cadastrar" component={Cadastrar} />
          <Route exact path="/dashboard" component={Dashboard} />
          <Route path="/sobre" component={Sobre} />
        </Switch>
      </section>
      <footer className="app-footer">
        <p>Made with love</p>
      </footer>
    </div>
  );
}

export default App;
