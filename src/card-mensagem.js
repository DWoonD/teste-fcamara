import React, { Component } from 'react';
import Moment from 'react-moment';


class CardMensagem extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <div className="card-mensagem">
            <p>{this.props.assunto}</p>
            <Moment format="DD/MM/YYYY HH:mm">
                {this.props.date}
            </Moment>
        </div>

    }
}

export default CardMensagem;