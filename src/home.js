import React from 'react';
import './style/home.css';

function Home() {
    return (
        <section className="home">
            <h1>Bem vindo ao teste que eu fiz para a FCamara</h1>
            <p>Este teste foi feito utilizando reactjs com css normal (acredito que de para fazer algumas configurações para poder usar
                o sass ou outro pré-processador, porém foquei nas lógicas e funcionalidades), um pouco de material-ui. 
            </p>
            <p>Busquei utilizar as melhores práticas no que foi pedido no teste, logo estou utilizando componentes com hooks, 
                fazendo rota com react-route-dom e chamada com axios (no momento só  menu está sendo chamado de um mock).
            </p>
            <p>O objetivo da aplicação é cadastrar algumas mensagens, as mesmas estão sendo salvas no localStorage do navegador, então
                 na tela de Dashboard, as mensagens são recuperadas e exibidas, podendo ser deletadas também.
            </p>
            <p>Acredito que poderia ter melhorado na organização do projeto como um todo, tentei separar o css, porém vi que tem 
                umas formas de usar dentro do próprio arquivo, mas como dito anteriormente o foco foi na lógica.
            </p>
            <p>Poderia também ter separado melhor os componentes em stateless e statefull, talvez aplicar redux entre outras coisas, 
                porém com o que fiz acredito que de para perceber que mesmo sendo algo novo para mim a vontade de aprender e ir atrás 
                é uma das minhas qualidades e não vejo problema em aprender coisas novas :D
            </p>
            <p>Foi uma grande experiência fazer isso em react totalmente do zero e gostei bastante da biblioteca, vou continuar 
                estudando e levando ela adiante pois achei bem simples e com grande potencial (afinal o mercado todo ama)
            </p>
            <p>Espero que aproveitem a experiência e meus contatos estão listado no sobre :D</p>
            <p>Agradeço demais a oportunidade.</p>
        </section>
    );
};

export default Home;